CREATE DATABASE IF NOT EXISTS cheetah;

USE cheetah;

CREATE TABLE IF NOT EXISTS usuarios(
	id int primary key auto_increment,
    nombre varchar(50),
    apellidos varchar(100),
    email varchar(150) not null,
    fecha_nacimiento TIMESTAMP DEFAULT CURRENT_TIMESTAMP(),
    nick varchar(50) not null,
    passwd varchar(50) not null,
    permiso varchar(50)
);

CREATE TABLE IF NOT EXISTS cliente(
	idcliente int primary key auto_increment,
    nombre varchar(50) not null,
    apellidos varchar(100) not null,
    dni varchar(100) not null,
    telefono int,
    fecha_nacimiento TIMESTAMP DEFAULT CURRENT_TIMESTAMP(),
    direccion varchar(900),
    email varchar(900),
    cod_postal int
);

CREATE TABLE IF NOT EXISTS vehiculo(
	idvehiculo int primary key auto_increment,
    modelo varchar(100) not null,
    marca varchar(50) not null,
	domicilio_veh varchar(200),
    tipo_vehiculo varchar(150) not null,
    potencia int,
    motor varchar(20),
    fecha_fabricacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP(),
    color varchar(50)
);

CREATE TABLE IF NOT EXISTS ventas(
	id int primary key auto_increment,
    tipo_consulta varchar(35),
    cod_venta int not null,
    iva int,
    fecha_venta TIMESTAMP DEFAULT CURRENT_TIMESTAMP(),
    precio_final float,
    descripcion varchar(150),
	observaciones varchar(250),
    id_cliente int UNSIGNED references cliente
);

CREATE TABLE IF NOT EXISTS revision(
	id int primary key auto_increment,
    cambio_radiador TIMESTAMP DEFAULT CURRENT_TIMESTAMP(),
    cambio_aceite TIMESTAMP DEFAULT CURRENT_TIMESTAMP(),
    cambio_filtro TIMESTAMP DEFAULT CURRENT_TIMESTAMP(),
    cambio_frenos TIMESTAMP DEFAULT CURRENT_TIMESTAMP(),
    cambio_neumaticos TIMESTAMP DEFAULT CURRENT_TIMESTAMP(),
    cambio_luces TIMESTAMP DEFAULT CURRENT_TIMESTAMP(),
    cambio_amortiguador TIMESTAMP DEFAULT CURRENT_TIMESTAMP(),
    id_vehiculo int unsigned references vehiculo
);

CREATE TABLE IF NOT EXISTS vehiculo_ventas(
	id_vehiculo int unsigned references vehiculo,
    id_ventas int unsigned references ventas,
    primary key(id_vehiculo,id_ventas)
);

delimiter ||
CREATE FUNCTION existeNombreUsuario(f_user varchar(50))
returns bit
begin
declare i int;
set i = 0;
    while ( i < (select max(id) from usuarios)) do
     if  ((select nick from usuarios where id = (i + 1)) like f_user) then return 1;
         end if;
    set i = i + 1;
    end while;
    return 0;
end; ||
delimiter ;

delimiter || 
CREATE FUNCTION existeDniCliente(f_dni varchar(9))
returns bit
begin
declare i int;
set i = 0;
    while ( i < (select max(idcliente) from cliente)) do
		if  ((select dni from cliente where idcliente = (i + 1)) like f_dni) then return 1;
         end if;
    set i = i + 1;
    end while;
    return 0;
end; ||
delimiter ;


delimiter || 
CREATE FUNCTION existeCodVenta(f_CodVenta int)
returns bit
begin
declare i int;
set i = 0;
    while ( i < (select max(id) from ventas)) do
    		if  ((select cod_venta from ventas where id = (i + 1)) like f_CodVenta) then return 1;
         end if;
    set i = i + 1;
    end while;
    return 0;
end; ||
delimiter ;
