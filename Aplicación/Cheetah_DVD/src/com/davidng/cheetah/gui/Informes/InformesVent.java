package com.davidng.cheetah.gui.Informes;

import com.davidng.cheetah.Ventas;
import com.davidng.cheetah.gui.Modelo;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.view.JasperViewer;

import java.util.HashMap;

/**
 * @author dnadeau
 * <p>
 * Clase donde tendremos los diferentes metodos para crear informes de ventas
 */
public class InformesVent {

    /**
     * Metodo con el que crearemos un informe con todos los registros de las ventas
     *
     * @param modelo Objeto de la clase Modelo
     */
    public void informVentCompleto(Modelo modelo) {

        try {
            JasperReport reporte = JasperCompileManager.compileReport("Informes_Jasper\\Ventas.jrxml");
            JasperPrint print = JasperFillManager.fillReport(reporte, null, modelo.conexion);
            JasperViewer viewer = new JasperViewer(print, false);
            viewer.show();
            JasperExportManager.exportReportToPdfFile(print, "Informes\\Totales\\Ventas.pdf");
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    /**
     * Metodo que nos creará un gráfico de los vehiculos más vendidos
     *
     * @param modelo Objeto de la clase Modelo
     */
    public void informVentGrafico(Modelo modelo) {

        try {
            JasperReport reporte = JasperCompileManager.compileReport("Informes_Jasper\\VehVenGrafico.jrxml");
            JasperPrint print = JasperFillManager.fillReport(reporte, null, modelo.conexion);
            JasperViewer viewer = new JasperViewer(print, false);
            viewer.show();
            JasperExportManager.exportReportToPdfFile(print, "Informes\\Graficos\\GraficoVentas.pdf");
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    /**
     * Metodo con el cual pasandole la venta como parámetro nos creará la correspondiente factura
     *
     * @param ventas Objeto de la clase Ventas
     * @param modelo Objeto de la clase Modelo
     * @param venta  Variable mediante la cual pasaremos la venta para crear la correspondiente factura de esta
     */
    public void informeFactura(int ventas, Modelo modelo, Ventas venta) {
        try {
            HashMap<String, Object> parametros = new HashMap<>();
            parametros.put("idventa", ventas);
            JasperReport reporte = JasperCompileManager.compileReport("Informes_Jasper\\Factura.jrxml");
            JasperPrint print = JasperFillManager.fillReport(reporte, parametros, modelo.conexion);
            JasperViewer viewer = new JasperViewer(print, false);
            viewer.show();
            JasperExportManager.exportReportToPdfFile(print, "Informes\\Facturas\\Factura" + venta.getId() + ".pdf");
        } catch (JRException e) {
            e.printStackTrace();
        }

    }
}
