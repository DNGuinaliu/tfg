package com.davidng.cheetah.gui.Ventanas.Vehiculos;

import javax.swing.*;
import java.awt.*;

/**
 * @author dnadeau
 * Clase donde encontraremos la ventana para generar informes de vehiculos
 */
public class InformesVehiculos extends JFrame{
    private JPanel panel1;
    public JButton informeTotal;
    public JButton informesGrafico;

    /**
     * Constructor de la clase donde se inicializaran los datos
     */
    public InformesVehiculos(){
        this.setTitle("CHEETAH");
        initDialog();
    }

    /**
     * Metodo donde inicializaremos la ventana
     */
    private void initDialog() {
        this.setContentPane(panel1);
        this.panel1.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        this.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
        this.pack();
        this.setSize(new Dimension(this.getWidth(), this.getHeight()));
        this.setVisible(false);
        this.setIconImage(getIconImage());
        this.setLocationRelativeTo(null);
    }

    /**
     * Metodo que nos mostrara un icono en la parte superior izquierda de la ventana
     * @return retValue
     */
    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().
                getImage(ClassLoader.getSystemResource("cheetah.png"));


        return retValue;
    }
}
