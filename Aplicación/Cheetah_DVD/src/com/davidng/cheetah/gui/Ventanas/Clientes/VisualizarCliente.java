package com.davidng.cheetah.gui.Ventanas.Clientes;

import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import java.awt.*;

/**
 * @author dnadeau
 * Clase donde se nos abrirá una ventana para poder visualizar los datos del cliente
 */
public class VisualizarCliente extends JFrame {
    private JPanel panel1;
    public JButton atrasButton;
    public JTextField txtNombre;
    public JTextField txtDNI;
    public JTextField txtEmail;
    public JTextField txtDireccion;
    public JTextField txtApellidos;
    public JTextField txtTelefono;
    public JTextField txtCP;
    public DatePicker fechaNacimiento;

    /**
     * Constructor de la clase donde se inicializaran los datos
     */
    public VisualizarCliente(){
        this.setTitle("CHEETAH");
        initDialog();
        getIconImage();
    }

    /**
     * Metodo donde inicializaremos la ventana
     */
    private void initDialog() {
        this.setContentPane(panel1);
        this.panel1.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        this.setDefaultCloseOperation(JDialog.EXIT_ON_CLOSE);
        this.pack();
        this.setSize(new Dimension(this.getWidth() + 500, this.getHeight() + 100));
        this.setVisible(false);
        this.setIconImage(getIconImage());
        this.setLocationRelativeTo(null);
    }

    /**
     * Metodo con el que se controlará la cantidad de caracteres introducidos en el campo
     * @return retValue
     */
    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().
                getImage(ClassLoader.getSystemResource("cheetah.png"));


        return retValue;
    }
}
