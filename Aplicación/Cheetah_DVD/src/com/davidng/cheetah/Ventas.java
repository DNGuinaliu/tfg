package com.davidng.cheetah;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class Ventas {
    private int id;
    private String tipoConsulta;
    private int codVenta;
    private int iva;
    private Date fechaVenta;
    private double precioFinal;
    private String descripcion;
    private String observaciones;
    private Cliente cliente;
    private List<Vehiculo> vehiculos;

    /**
     * Constructor vacio de la clase Ventas
     */
    public Ventas() {
    }

    /**
     * Constructor de la clase Ventas donde se inicializaran los datos menos la id
     *
     * @param tipoConsulta
     * @param codVenta
     * @param iva
     * @param fechaVenta
     * @param precioFinal
     * @param descripcion
     * @param observaciones
     * @param cliente
     */
    public Ventas(String tipoConsulta, int codVenta, int iva, Date fechaVenta, double precioFinal, String descripcion, String observaciones, Cliente cliente) {
        this.tipoConsulta = tipoConsulta;
        this.codVenta = codVenta;
        this.iva = iva;
        this.fechaVenta = fechaVenta;
        this.precioFinal = precioFinal;
        this.descripcion = descripcion;
        this.observaciones = observaciones;
        this.cliente = cliente;
    }

    /**
     * Constructor de la clase Ventas donde se inicializaran los datos
     *
     * @param id
     * @param tipoConsulta
     * @param codVenta
     * @param iva
     * @param fechaVenta
     * @param precioFinal
     * @param descripcion
     * @param observaciones
     * @param cliente
     */
    public Ventas(int id, String tipoConsulta, int codVenta, int iva, Date fechaVenta, double precioFinal, String descripcion, String observaciones, Cliente cliente) {
        this.id = id;
        this.tipoConsulta = tipoConsulta;
        this.codVenta = codVenta;
        this.iva = iva;
        this.fechaVenta = fechaVenta;
        this.precioFinal = precioFinal;
        this.descripcion = descripcion;
        this.observaciones = observaciones;
        this.cliente = cliente;
    }

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "tipo_consulta")
    /**
     * Metodo con el que devolvemos el tipo de consulta
     */
    public String getTipoConsulta() {
        return tipoConsulta;
    }

    /**
     * Metodo con el que dar valor al tipo de consulta
     *
     * @param tipoConsulta Variable con la que introduciremos el tipo de consulta
     */
    public void setTipoConsulta(String tipoConsulta) {
        this.tipoConsulta = tipoConsulta;
    }

    @Basic
    @Column(name = "cod_venta")
    /**
     * Metodo con el que devolvemos el codigo de venta
     */
    public int getCodVenta() {
        return codVenta;
    }

    /**
     * Metodo con el que dar valor al codigo de venta
     *
     * @param codVenta Variable con la que introduciremos el codigo de venta
     */
    public void setCodVenta(int codVenta) {
        this.codVenta = codVenta;
    }

    @Basic
    @Column(name = "iva")
    /**
     * Metodo con el que devolvemos el iva
     */
    public int getIva() {
        return iva;
    }

    /**
     * Metodo con el que dar valor al iva
     *
     * @param iva Variable con la que introduciremos el iva
     */
    public void setIva(int iva) {
        this.iva = iva;
    }

    @Basic
    @Column(name = "fecha_venta")
    /**
     * Metodo con el que devolvemos la fecha de venta
     */
    public Date getFechaVenta() {
        return fechaVenta;
    }

    /**
     * Metodo con el que dar valor a la fecha de venta
     *
     * @param fechaVenta Variable con la que introduciremos la fecha de venta
     */
    public void setFechaVenta(Date fechaVenta) {
        this.fechaVenta = fechaVenta;
    }

    @Basic
    @Column(name = "precio_final")
    /**
     * Metodo con el que devolvemos el precio
     */
    public double getPrecioFinal() {
        return precioFinal;
    }

    /**
     * Metodo con el que dar valor al precio
     *
     * @param precioFinal Variable con la que introduciremos el precio
     */
    public void setPrecioFinal(double precioFinal) {
        this.precioFinal = precioFinal;
    }

    @Basic
    @Column(name = "descripcion")
    /**
     * Metodo con el que devolvemos la descripción
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * Metodo con el que dar valor a la descripción
     *
     * @param descripcion Variable con la que introduciremos la descripción
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Basic
    @Column(name = "observaciones")
    /**
     * Metodo con el que devolvemos la observación
     */
    public String getObservaciones() {
        return observaciones;
    }

    /**
     * Metodo con el que dar valor a la observación
     *
     * @param observaciones Variable con la que introduciremos la observación
     */
    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ventas ventas = (Ventas) o;
        return id == ventas.id &&
                codVenta == ventas.codVenta &&
                iva == ventas.iva &&
                Double.compare(ventas.precioFinal, precioFinal) == 0 &&
                Objects.equals(tipoConsulta, ventas.tipoConsulta) &&
                Objects.equals(fechaVenta, ventas.fechaVenta) &&
                Objects.equals(descripcion, ventas.descripcion) &&
                Objects.equals(observaciones, ventas.observaciones);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, tipoConsulta, codVenta, iva, fechaVenta, precioFinal, descripcion, observaciones);
    }

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "id_cliente", referencedColumnName = "idcliente")
    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @ManyToMany(mappedBy = "ventas", fetch = FetchType.EAGER)
    public List<Vehiculo> getVehiculos() {
        return vehiculos;
    }

    public void setVehiculos(List<Vehiculo> vehiculos) {
        this.vehiculos = vehiculos;
    }

    @Override
    public String toString() {
        if (cliente == null) {
            return "No existe Cliente" +
                    ", " + vehiculos;
        } else {
            return cliente +
                    ", " + vehiculos;
        }

    }
}
