package com.davidng.cheetah;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
public class Revision {
    private int id;
    private Date cambioRadiador;
    private Date cambioAceite;
    private Date cambioFiltro;
    private Date cambioFrenos;
    private Date cambioNeumaticos;
    private Date cambioLuces;
    private Date cambioAmortiguador;
    private Vehiculo vehiculo;

    /**
     * Constructor vacio de la clase Revision
     */
    public Revision() {
    }

    /**
     * Constructor de la clase Revision donde se inicializaran los datos menos la id
     *
     * @param cambioRadiador
     * @param cambioAceite
     * @param cambioFiltro
     * @param cambioFrenos
     * @param cambioNeumaticos
     * @param cambioLuces
     * @param cambioAmortiguador
     * @param vehiculo
     */
    public Revision(Date cambioRadiador, Date cambioAceite, Date cambioFiltro, Date cambioFrenos, Date cambioNeumaticos, Date cambioLuces, Date cambioAmortiguador, Vehiculo vehiculo) {
        this.cambioRadiador = cambioRadiador;
        this.cambioAceite = cambioAceite;
        this.cambioFiltro = cambioFiltro;
        this.cambioFrenos = cambioFrenos;
        this.cambioNeumaticos = cambioNeumaticos;
        this.cambioLuces = cambioLuces;
        this.cambioAmortiguador = cambioAmortiguador;
        this.vehiculo = vehiculo;
    }

    /**
     * Constructor de la clase Revision donde se inicializaran los datos
     *
     * @param id
     * @param cambioRadiador
     * @param cambioAceite
     * @param cambioFiltro
     * @param cambioFrenos
     * @param cambioNeumaticos
     * @param cambioLuces
     * @param cambioAmortiguador
     * @param vehiculo
     */
    public Revision(int id, Date cambioRadiador, Date cambioAceite, Date cambioFiltro, Date cambioFrenos, Date cambioNeumaticos, Date cambioLuces, Date cambioAmortiguador, Vehiculo vehiculo) {
        this.id = id;
        this.cambioRadiador = cambioRadiador;
        this.cambioAceite = cambioAceite;
        this.cambioFiltro = cambioFiltro;
        this.cambioFrenos = cambioFrenos;
        this.cambioNeumaticos = cambioNeumaticos;
        this.cambioLuces = cambioLuces;
        this.cambioAmortiguador = cambioAmortiguador;
        this.vehiculo = vehiculo;
    }

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "cambio_radiador")
    /**
     * Metodo con el que devolvemos el cambio de radiador
     */
    public Date getCambioRadiador() {
        return cambioRadiador;
    }

    /**
     * Metodo con el que dar valor al cambio de radiador
     *
     * @param cambioRadiador Variable con la que introduciremos el cambio de radiador
     */
    public void setCambioRadiador(Date cambioRadiador) {
        this.cambioRadiador = cambioRadiador;
    }

    @Basic
    @Column(name = "cambio_aceite")
    /**
     * Metodo con el que devolvemos el cambio de aceite
     */
    public Date getCambioAceite() {
        return cambioAceite;
    }

    /**
     * Metodo con el que dar valor al cambio de aceite
     *
     * @param cambioAceite Variable con la que introduciremos el cambio de aceite
     */
    public void setCambioAceite(Date cambioAceite) {
        this.cambioAceite = cambioAceite;
    }

    @Basic
    @Column(name = "cambio_filtro")
    /**
     * Metodo con el que devolvemos el cambio de filtro
     */
    public Date getCambioFiltro() {
        return cambioFiltro;
    }

    /**
     * Metodo con el que dar valor al cambio de filtro
     *
     * @param cambioFiltro Variable con la que introduciremos el cambio de filtro
     */
    public void setCambioFiltro(Date cambioFiltro) {
        this.cambioFiltro = cambioFiltro;
    }

    @Basic
    @Column(name = "cambio_frenos")
    /**
     * Metodo con el que devolvemos el cambio de frenos
     */
    public Date getCambioFrenos() {
        return cambioFrenos;
    }

    /**
     * Metodo con el que dar valor al cambio de frenos
     *
     * @param cambioFrenos Variable con la que introduciremos el cambio de frenos
     */
    public void setCambioFrenos(Date cambioFrenos) {
        this.cambioFrenos = cambioFrenos;
    }

    @Basic
    @Column(name = "cambio_neumaticos")
    /**
     * Metodo con el que devolvemos el cambio de neumaticos
     */
    public Date getCambioNeumaticos() {
        return cambioNeumaticos;
    }

    /**
     * Metodo con el que dar valor al cambio de neumaticos
     *
     * @param cambioNeumaticos Variable con la que introduciremos el cambio de neumaticos
     */
    public void setCambioNeumaticos(Date cambioNeumaticos) {
        this.cambioNeumaticos = cambioNeumaticos;
    }

    @Basic
    @Column(name = "cambio_luces")
    /**
     * Metodo con el que devolvemos el cambio de luces
     */
    public Date getCambioLuces() {
        return cambioLuces;
    }

    /**
     * Metodo con el que dar valor al cambio de luces
     *
     * @param cambioLuces Variable con la que introduciremos el cambio de luces
     */
    public void setCambioLuces(Date cambioLuces) {
        this.cambioLuces = cambioLuces;
    }

    @Basic
    @Column(name = "cambio_amortiguador")
    /**
     * Metodo con el que devolvemos el cambio de amortiguador
     */
    public Date getCambioAmortiguador() {
        return cambioAmortiguador;
    }

    /**
     * Metodo con el que dar valor al cambio de amortiguador
     *
     * @param cambioAmortiguador Variable con la que introduciremos el cambio de amortiguador
     */
    public void setCambioAmortiguador(Date cambioAmortiguador) {
        this.cambioAmortiguador = cambioAmortiguador;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Revision revision = (Revision) o;
        return id == revision.id &&
                Objects.equals(cambioRadiador, revision.cambioRadiador) &&
                Objects.equals(cambioAceite, revision.cambioAceite) &&
                Objects.equals(cambioFiltro, revision.cambioFiltro) &&
                Objects.equals(cambioFrenos, revision.cambioFrenos) &&
                Objects.equals(cambioNeumaticos, revision.cambioNeumaticos) &&
                Objects.equals(cambioLuces, revision.cambioLuces) &&
                Objects.equals(cambioAmortiguador, revision.cambioAmortiguador);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, cambioRadiador, cambioAceite, cambioFiltro, cambioFrenos, cambioNeumaticos, cambioLuces, cambioAmortiguador);
    }

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "id_vehiculo", referencedColumnName = "idvehiculo")
    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    @Override
    public String toString() {
        if (vehiculo != null) {
            return vehiculo + "";
        } else {
            return "Vehiculo eliminado";
        }
    }
}
