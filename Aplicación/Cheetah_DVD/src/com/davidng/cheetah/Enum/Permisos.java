package com.davidng.cheetah.Enum;

/**
 * @author dnadeau
 * Clase enumerada donde tendremos creados los tipos de permisos que podrá tener un usuario
 */
public enum Permisos {
    ADMINISTRADOR("Administrador"),
    VENDEDOR("Vendedor"),
    JEFE("Jefe");

    private String permiso; //Variable donde se recogerán los datos enumerados anteriores

    /**
     * Constructor de la clase Permisos donde se inicializa la variable String
     * @param permiso Variable donde se recogerán los datos enumerados anteriores
     */
    Permisos(String permiso) {
        this.permiso = permiso;
    }

    /**
     * Metodo que nos devolvera el permiso
     * @return permiso
     */
    public String getPermiso() {
        return permiso;
    }
}
