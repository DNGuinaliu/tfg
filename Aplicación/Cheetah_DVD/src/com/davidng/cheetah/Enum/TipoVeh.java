package com.davidng.cheetah.Enum;

/**
 * @author dnadeau
 * Clase enumerada donde tendremos creados los tipos de vehiculos
 */
public enum TipoVeh {
    COMPACTO("Compacto"),
    SEDAN("Sedán"),
    FAMILIAR("Familiar"),
    COUPE("Coupé"),
    CABRIOLET("Cabriolet"),
    TODOTERRENO("4X4"),
    MONOVOLUMEN("Monovolumen"),
    PICKUP("Pick Up"),
    UTILITARIO("Utilitario");


    private String tipveh;//Variable donde se recogerán los datos enumerados anteriores

    /**
     * Constructor de la clase TipoVeh donde se inicializa la variable String
     * @param tipveh Variable donde se recogerán los datos enumerados anteriores
     */
    TipoVeh(String tipveh) {
        this.tipveh = tipveh;
    }

    /**
     * Metodo que nos devolvera el tipo de vehiculo
     * @return tipveh
     */
    public String getTipveh() {
        return tipveh;
    }
}
