package com.davidng.cheetah.gui.Listados;

import com.davidng.cheetah.*;
import com.davidng.cheetah.gui.Ventanas.Administrador;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dnadeau
 * Clase donde tendremos los diferentes metodos para listar los datos de cada clase
 */
public class Listados {

    /**
     * Metodo con el que mostraremos los datos de los usuarios
     *
     * @param usuario       ArrayList de la clase usuario
     * @param administrador Objeto de la clase administrador
     */
    public void listarUsuarios(ArrayList<Usuarios> usuario, Administrador administrador) {
        administrador.dlmUsuarios.clear();
        for (Usuarios unUsuario : usuario) {
            administrador.dlmUsuarios.addElement(unUsuario);
        }
    }

    /**
     * Metodo con el que mostraremos los datos de los clientes
     *
     * @param cliente       ArrayList de la clase cliente
     * @param administrador Objeto de la clase administrador
     */
    public void listarClientes(ArrayList<Cliente> cliente, Administrador administrador) {
        administrador.dlmClientes.clear();
        for (Cliente unCliente : cliente) {
            administrador.dlmClientes.addElement(unCliente);
        }
    }

    /**
     * Metodo con el que mostraremos los datos de los vehiculos
     *
     * @param vehiculo      ArrayList de la clase vehiculo
     * @param administrador Objeto de la clase administrador
     */
    public void listarVehiculos(ArrayList<Vehiculo> vehiculo, Administrador administrador) {
        administrador.dlmVehiculos.clear();
        for (Vehiculo unVehiculo : vehiculo) {
            administrador.dlmVehiculos.addElement(unVehiculo);
        }
    }

    /**
     * Metodo con el que mostraremos los datos de las revisiones
     *
     * @param revision      ArrayList de la clase revision
     * @param administrador Objeto de la clase administrador
     */
    public void listarRevision(ArrayList<Revision> revision, Administrador administrador) {
        administrador.dlmRevision.clear();
        for (Revision unaRevision : revision) {
            administrador.dlmRevision.addElement(unaRevision);
        }
    }

    /**
     * Metodo con el que mostraremos los datos de las ventas
     *
     * @param ventas        ArrayList de la clase ventas
     * @param administrador Objeto de la clase administrador
     */
    public void listarVentas(ArrayList<Ventas> ventas, Administrador administrador) {
        administrador.dlmVenta.clear();
        for (Ventas unaVenta : ventas) {
            administrador.dlmVenta.addElement(unaVenta);
        }
    }


    /**
     * Metodo con el que se podran visualizar los vehiculos que se pueden vender
     * @param listaTodos List de la clase Vehiculo
     * @param administrador Objeto de la clase administrador
     */
    public void listarVehiculosVenta(List<Vehiculo> listaTodos, Administrador administrador) {
        Ventas tienda = (Ventas) administrador.listaVenta.getSelectedValue();
        administrador.dlmVentaVehiculo.clear();

        listaTodos.removeAll(tienda.getVehiculos());

        for (Vehiculo vehiculo : listaTodos) {
            administrador.dlmVentaVehiculo.addElement(vehiculo);
        }
    }

    /**
     * Metodo con el que seleccionando un vehiculo nos listara la venta en la que ha sido incluido el vehiculo
     * @param administrador Objeto de la clase administrador
     */
    public void listarVentaVehiculos(Administrador administrador) {
        Vehiculo vehiculo = (Vehiculo) administrador.listaVehiculosAdmin.getSelectedValue();
        administrador.dlmVehiculoVenta.clear();
        for (Ventas ventas : vehiculo.getVentas()) {
            administrador.dlmVehiculoVenta.addElement(ventas);
        }

    }
}
