package com.davidng.cheetah.gui.Ventanas.Vehiculos;

import com.davidng.cheetah.Enum.Marcas;
import com.davidng.cheetah.Enum.Motor;
import com.davidng.cheetah.Enum.TipoVeh;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import java.awt.*;

/**
 * @author dnadeau
 * Clase donde se nos abrirá una ventana para poder visualizar los datos del cliente
 */
public class VisualizarVeh extends JFrame {

    private JPanel panel1;
    public JButton atrasButton;
    public JComboBox cbMarca;
    public JComboBox cbMotor;
    public JTextField txtPotencia;
    public JTextField txtColor;
    public JTextField txtDomicilio;
    public JTextField txtModelo;
    public DatePicker fechaFabricacion;
    public JComboBox cbTipoVeh;

    /**
     * Constructor de la clase donde se inicializaran los datos
     */
    public VisualizarVeh() {
        this.setTitle("CHEETAH");
        initDialog();
        getIconImage();
        setComboBoxDatosVehiculos();
    }


    /**
     * Metodo donde inicializaremos la ventana
     */
    private void initDialog() {
        this.setContentPane(panel1);
        this.panel1.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        this.setDefaultCloseOperation(JDialog.EXIT_ON_CLOSE);
        this.pack();
        this.setSize(new Dimension(this.getWidth() + 500, this.getHeight() + 100));
        this.setVisible(false);
        this.setIconImage(getIconImage());
        this.setLocationRelativeTo(null);
    }

    /**
     * Metodo con el que se controlará la cantidad de caracteres introducidos en el campo
     * @return retValue
     */
    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().
                getImage(ClassLoader.getSystemResource("cheetah.png"));


        return retValue;
    }

    /**
     * Metodo con el que introduciremos los datos de las clases enumeradas en las combobox
     */
    private void setComboBoxDatosVehiculos() {
        for (Marcas marcas : Marcas.values()) {
            cbMarca.addItem(marcas.getMarca());
        }
        cbMarca.setSelectedItem(0);

        for (Motor motor : Motor.values()) {
            cbMotor.addItem(motor.getMotor());
        }
        cbMotor.setSelectedItem(0);

        for (TipoVeh tipoVeh : TipoVeh.values()) {
            cbTipoVeh.addItem(tipoVeh.getTipveh());
        }
        cbTipoVeh.setSelectedItem(0);
    }
}
