package com.davidng.cheetah.gui.Ventanas.Vehiculos;

import com.davidng.cheetah.Enum.Marcas;
import com.davidng.cheetah.Enum.Motor;
import com.davidng.cheetah.Enum.TipoVeh;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * @author dnadeau
 * Clase donde tendremos la ventana para crear un nuevo vehiculo
 */
public class NuevoVehiculo extends JFrame {
    private JPanel panelNuvVeh;
    public JButton atrasVehButton;
    public JButton nuevoVehButton;

    public JTextField txtModeloVeh;
    public JTextField txtDomicilio;
    public JTextField txtColor;
    public JTextField txtPotencia;

    public DatePicker fechaFabriVeh;

    public JComboBox cbMotor;
    public JComboBox cbMarcaVeh;
    public JComboBox cbTipoVeh;

    /**
     * Constructor de la clase donde se inicializaran los datos
     */
    public NuevoVehiculo() {
        this.setTitle("CHEETAH");
        initDialog();
        setComboBoxDatosVehiculos();
        controlCantCaracteres();
    }

    /**
     * Metodo donde inicializaremos la ventana
     */
    private void initDialog() {
        this.setContentPane(panelNuvVeh);
        this.panelNuvVeh.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        this.setDefaultCloseOperation(JDialog.EXIT_ON_CLOSE);
        this.pack();
        this.setSize(new Dimension(this.getWidth() + 500, this.getHeight() + 100));
        this.setVisible(false);
        this.setIconImage(getIconImage());
        this.setLocationRelativeTo(null);
    }

    /**
     * Metodo con el que introduciremos los datos de las clases enumeradas en las combobox
     */
    private void setComboBoxDatosVehiculos() {
        for (Marcas marcas : Marcas.values()) {
            cbMarcaVeh.addItem(marcas.getMarca());
        }
        cbMarcaVeh.setSelectedItem(-1);

        for (Motor motor : Motor.values()) {
            cbMotor.addItem(motor.getMotor());
        }
        cbMotor.setSelectedItem(-1);

        for (TipoVeh tipoVeh : TipoVeh.values()) {
            cbTipoVeh.addItem(tipoVeh.getTipveh());
        }
        cbTipoVeh.setSelectedItem(0);
    }

    /**
     * Metodo que nos mostrara un icono en la parte superior izquierda de la ventana
     *
     * @return retValue
     */
    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().
                getImage(ClassLoader.getSystemResource("cheetah.png"));


        return retValue;
    }

    /**
     * Metodo con el que introduciremos los datos de las clases enumeradas en las combobox
     */
    private void controlCantCaracteres() {
        txtModeloVeh.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent k) {
                if (txtModeloVeh.getText().length() > 99) {
                    k.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        txtDomicilio.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent k) {
                if (txtDomicilio.getText().length() > 99) {
                    k.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        txtColor.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent k) {
                if (txtColor.getText().length() > 49) {
                    k.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
    }
}
