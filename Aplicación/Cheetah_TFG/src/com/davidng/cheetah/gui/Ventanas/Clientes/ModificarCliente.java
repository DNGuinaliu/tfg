package com.davidng.cheetah.gui.Ventanas.Clientes;

import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * @author dnadeau
 * Clase en la cual podremos modificar un cliente mediante una ventana
 */
public class ModificarCliente extends JFrame {
    private JPanel panelModClien;
    public JButton atrasModClienButton;
    public JButton guardarClientButton;

    public JTextField txtNombreClienMod;
    public JTextField txtApeClienMod;
    public JTextField txtDniClienMod;
    public JTextField txtEmailClienMod;
    public JTextField txtDireCienMod;
    public JTextField txtCPClienMod;
    public JTextField txtTelClienMod;


    public DatePicker FechaNacClienMod;

    /**
     * Constructor de la clase donde se inicializaran los datos
     */
    public ModificarCliente() {
        this.setTitle("CHEETAH");
        initDialog();
        controlCantCaracteres();
    }

    /**
     * Metodo donde inicializaremos la ventana
     */
    private void initDialog() {
        this.setContentPane(panelModClien);
        this.panelModClien.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        this.setDefaultCloseOperation(JDialog.EXIT_ON_CLOSE);
        this.pack();
        this.setSize(new Dimension(this.getWidth() + 500, this.getHeight() + 100));
        this.setVisible(false);
        this.setIconImage(getIconImage());
        this.setLocationRelativeTo(null);
    }

    /**
     * Metodo con el que se controlará la cantidad de caracteres introducidos en el campo
     */
    private void controlCantCaracteres() {
        txtNombreClienMod.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent k) {
                if (txtNombreClienMod.getText().length() > 49) {
                    k.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        txtApeClienMod.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent k) {
                if (txtApeClienMod.getText().length() > 99) {
                    k.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        txtDniClienMod.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent k) {
                if (txtDniClienMod.getText().length() > 8) {
                    k.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        txtEmailClienMod.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent k) {
                if (txtEmailClienMod.getText().length() > 99) {
                    k.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        txtDireCienMod.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent k) {
                if (txtDireCienMod.getText().length() > 99) {
                    k.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
    }

    /**
     * Metodo que nos mostrara un icono en la parte superior izquierda de la ventana
     *
     * @return retValue
     */
    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().
                getImage(ClassLoader.getSystemResource("cheetah.png"));


        return retValue;
    }
}
