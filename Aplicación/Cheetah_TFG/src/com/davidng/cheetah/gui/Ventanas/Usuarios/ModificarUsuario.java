package com.davidng.cheetah.gui.Ventanas.Usuarios;

import com.davidng.cheetah.Enum.Permisos;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * @author dnadeau
 * Clase en la cual podremos modificar un usuario mediante una ventana
 */
public class ModificarUsuario extends JFrame {
    private JPanel panellModificarUsuario;
    public JButton guardarUserModButton;
    public JButton atrasModUserButton;

    public JTextField txtNombUserMod;
    public JTextField txtApellMod;
    public JTextField txtEmailMod;
    public JTextField txtNickMod;

    public JComboBox cbPermMod;

    public DatePicker fechaNacModUser;

    public JPasswordField txtContrMod;

    /**
     * Constructor de la clase donde se inicializaran los datos
     */
    public ModificarUsuario() {
        this.setTitle("CHEETAH");
        initDialog();
        setComboBoxDatosUser();
        controlCantCaracteres();
    }

    /**
     * Metodo donde inicializaremos la ventana
     */
    private void initDialog() {
        this.setContentPane(panellModificarUsuario);
        this.panellModificarUsuario.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        this.setDefaultCloseOperation(JDialog.EXIT_ON_CLOSE);
        this.pack();
        this.setSize(new Dimension(this.getWidth() + 500, this.getHeight() + 100));
        this.setVisible(false);
        this.setIconImage(getIconImage());
        this.setLocationRelativeTo(null);
    }

    /**
     * Metodo con el que introducimos los datos de la clase enumerada Permisos
     * a la combobox
     */
    private void setComboBoxDatosUser() {
        for (Permisos permisos : Permisos.values()) {
            cbPermMod.addItem(permisos.getPermiso());
        }
        cbPermMod.setSelectedItem(-1);
    }

    /**
     * Metodo con el que se controlará la cantidad de caracteres introducidos en el campo
     */
    private void controlCantCaracteres() {
        txtNombUserMod.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent k) {
                if (txtNombUserMod.getText().length() > 49) {
                    k.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        txtApellMod.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent k) {
                if (txtApellMod.getText().length() > 99) {
                    k.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        txtEmailMod.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent k) {
                if (txtEmailMod.getText().length() > 149) {
                    k.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        txtNickMod.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent k) {
                if (txtNickMod.getText().length() > 49) {
                    k.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        txtContrMod.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent k) {
                if (txtContrMod.getText().length() > 49) {
                    k.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
    }

    /**
     * Metodo que nos mostrara un icono en la parte superior izquierda de la ventana
     *
     * @return retValue
     */
    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().
                getImage(ClassLoader.getSystemResource("cheetah.png"));


        return retValue;
    }
}
