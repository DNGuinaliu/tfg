package com.davidng.cheetah.gui.Informes;

import com.davidng.cheetah.gui.Modelo;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.view.JasperViewer;

import java.util.HashMap;

/**
 * @author dnadeau
 * Clase donde tendremos los diferentes metodos para crear informes de vehiculos
 */
public class InformesVeh {

    /**
     * Metodo con el que crearemos un informe con todos los registros de los vehiculos
     *
     * @param modelo Objeto de la clase Modelo
     */
    public void informeVehiculoCompleto(Modelo modelo) {

        try {
            JasperReport reporte = JasperCompileManager.compileReport("src\\Informes\\Vehiculos.jrxml");
            JasperPrint print = JasperFillManager.fillReport(reporte, null, modelo.conexion);
            JasperViewer viewer = new JasperViewer(print, false);
            viewer.show();

            JasperExportManager.exportReportToPdfFile(print, "Informes\\Totales\\Vehiculo.pdf");
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    /**
     * Metodo donde se creara un informe con un gráfico de vehiculos
     * @param modelo Objeto de la clase Modelo
     */
    public void informeVehiculoGrafico(Modelo modelo) {
        try {
            JasperReport reporte = JasperCompileManager.compileReport("src\\Informes\\VehGrafico.jrxml");
            JasperPrint print = JasperFillManager.fillReport(reporte, null, modelo.conexion);
            JasperViewer viewer = new JasperViewer(print, false);
            viewer.show();

            JasperExportManager.exportReportToPdfFile(print, "Informes\\Graficos\\Vehiculo.pdf");
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }
}
