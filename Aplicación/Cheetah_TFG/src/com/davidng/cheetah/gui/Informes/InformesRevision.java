package com.davidng.cheetah.gui.Informes;

import com.davidng.cheetah.gui.Modelo;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.view.JasperViewer;

/**
 * @author dnadeau
 * Clase donde tendremos los diferentes metodos para crear informes de revisión
 */
public class InformesRevision {

    /**
     * Metodo con el que crearemos un informe con todos los registros de las revisiones
     * @param modelo Objeto de la clase Modelo
     */
    public void informRevCompleto(Modelo modelo){

        try {
            JasperReport reporte = JasperCompileManager.compileReport("src\\Informes\\Revision.jrxml");
            JasperPrint print = JasperFillManager.fillReport(reporte, null, modelo.conexion);
            JasperViewer viewer = new JasperViewer(print,false);
            viewer.show();

            JasperExportManager.exportReportToPdfFile(print, "Informes\\Totales\\Revision.pdf");
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }
}
